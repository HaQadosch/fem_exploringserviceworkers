'use strict'

var curFib = 0

self.onmessage = onMessage

function getNextFib () {
  self.postMessage({ num: curFib, fib: fib(curFib) })
  curFib++
  setTimeout(getNextFib, 0)
}

function fib (n) {
  if (n < 2) {
    return n
  }
  return fib(n - 1) + fib(n - 2)
}

function onMessage ({ data = 'workerDefault' }) {
  getNextFib()
}
