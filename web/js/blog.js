(function Blog () {
  'use strict'

  // let isOnline = navigator?.onLine ?? true
  // const isLoggedIn = /isLoggedIn=1/.test(document.cookie.toString() || '')
  // const isServiceWorkerInNavigator = ('serviceWorker' in navigator)
  // let blogWorker

  document.addEventListener('DOMContentLoaded', ready, false)

  initServiceWorker().catch(console.error)

  // **********************************

  function ready () {
    let isOnline = true
    const offlineIcon = document.getElementById('connectivity-status')
    if (!isOnline) {
      offlineIcon.classList.remove('hidden')
    }

    window.addEventListener('online', function onOnline () {
      offlineIcon.classList.add('hidden')
      isOnline = true
    })

    window.addEventListener('offline', function onOffline () {
      offlineIcon.classList.remove('hidden')
      isOnline = false
    })
  }

  async function initServiceWorker () {
    console.log({ initServiceWorker: true })
    const serviceWorkerRegistration = await navigator.serviceWorker.register(
      '/sw.js',
      {
        updateViaCache: 'none'
      })

    let blogWorker = serviceWorkerRegistration.installing ||
      serviceWorkerRegistration.waiting ||
      serviceWorkerRegistration.active

    navigator.serviceWorker.addEventListener('controllerchange', function onControllerChange () {
      blogWorker = navigator.serviceWorker.controller
    })
  }
})()
