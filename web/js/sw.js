'use strict'

const version = '1.0.0'

self.addEventListener('install', onInstall)
self.addEventListener('activate', onActivate)

main().catch(console.error)

async function main () {
  console.log({ serviceWorkerMain: version })
}

async function onInstall () {
  console.log({ serviceWorkerInstall: version })
  self.skipWaiting()
}

function onActivate (evt) {
  console.log({ serviceWorkerActivate: version })
  evt.waitUntil(handleActivation())
}

async function handleActivation () {
  console.log({ serviceWorkerClientsClaimed: version })
  await clients.claim()
}
